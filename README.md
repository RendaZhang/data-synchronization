## 同步 MySQL 数据库到 Couchbase 数据库方案



### 背景



分布式数据库：

- 由位于不同站点的多个数据文件组成
- 允许多个用户访问和操作数据
- 从离用户最近的位置快速传送文件
- 如果其中一个站点发生故障，数据可以恢复
- 来自分散数据库的多个文件必须同步

集中式数据库：

- 由单个中央数据库文件组成
- 多个用户同时访问同一文件时发生瓶颈问题
- 文件传递给用户可能需要更长时间
- 单站点意味着系统发生故障时停机
- 在单一的中央系统中更简单地更新和管理数据



分布式数据库的核心 —— 数据分片、数据同步。



数据分片：

该特性是分布式数据库的技术创新，基于分布式理论。它可以突破中心化数据库单机的容量限制，从而将数据分散到多节点，以更灵活、高效的方式来处理数据。

分片方式包括两种。

- 水平分片 - 按行进行数据分割，数据被切割为一个个数据组，分散到不同节点上。

- 垂直分片 - 按列进行数据切割，一个数据表的模式（Schema）被切割为多个小的模式。



数据同步：

它是分布式数据库的底线。由于数据库理论传统上是建立在单机数据库基础上，而引入分布式理论后，一致性原则被打破。因此需要引入数据库同步技术来帮助数据库恢复一致性。

简而言之，就是使分布式数据库用起来像“正常的数据库”。所以数据同步背后的推动力，就是人们对数据“一致性”的追求。这两个概念相辅相成，互相作用。



相关的 MySQL 语句：insert, update, delete





****

### 全量同步方式

#### 1. 普通方式

分页查询 MySQL 端的表，然后通过 JDBC 的 batch 方式插入到 Couchbase 中；分页查询时，需要按照主键 ID 来排序分页，避免重复插入。



#### 2. 基于 BulkLoad 的数据同步

使用 spark 任务，通过 HQl 读取数据，然后再通过 Couchbase 的 Api 插入到 Couchbase 中。





****

### 增量同步方式



#### 1. 使用触发器实时同步数据

优点是可以实时同步，缺点是一次只能同步一条语句。

- 基于原表创触发器，触发器包含 insert，modify，delete 三种类型的操作，数据库的触发器分 Before 和 After 两种情况，一种是在 insert，modify，delete 三种类型的操作发生之前触发（比如记录日志操作，一般是 Before），一种是在 insert，modify，delete 三种类型的操作之后触发。
- 创建增量表，增量表中的字段和原表中的字段完全一样，但是需要多一个操作类型字段（分表代表 insert，modify，delete 三种类型的操作），并且需要一个唯一自增 ID，代表数据原表中数据操作的顺序，这个自增 id 非常重要，不然数据同步就会错乱。
- 原表中出现 insert，modify，delete 三种类型的操作时，通过触发器自动产生增量数据，插入增量表中。
- 处理增量表中的数据，处理时，一定是按照自增 id 的顺序来处理，这种效率会非常低，没办法做批量操作，不然数据会错乱。 数据的增删改是有顺序的，而且需要确保同一条数据的增删改顺序不可以出错。



#### 2. 基于时间戳的增量同步

优点是一次可以批量同步，缺点是有延迟。

使用 ETL 工具。

- 首先需要一张临时 temp 表，用来存取每次读取的待同步的数据，也就是把每次从原表中根据时间戳读取到数据先插入到临时表中，每次在插入前，先清空临时表的数据。
- 还需要创建一个时间戳配置表，用于存放每次读取的处理完的数据的最后的时间戳。
- 每次从原表中读取数据时，先查询时间戳配置表，然后就知道了查询原表时的开始时间戳。
- 根据时间戳读取到原表的数据，插入到临时表中，然后再将临时表中的数据插入到目标表中。

- 从 temp 表中读取出数据的最大时间戳，并且更新到时间戳配置表中。临时 temp 表的作用就是使用 SQL 获取每次读取到的数据的最大的时间戳，当然这些都是完全基于 SQL 语句在 Kettle 中来配置，才需要这样的一张 temp 表。



#### 3. 基于数据库日志的同步

使用 MySQL 的 binlog。

MySQL 的主从同步：

- Master 将改变记录到二进制日志 binary log 中（这些记录叫做二进制日志事件，binary log events，可以通过 show binlog events 进行查看）；
- Slave 将 master 的 binary log events 拷贝到它的中继日志 (relay log)；
- Slave 重做中继日志中的事件，将改变反映它自己的数据。

阿里巴巴开源的 Canal 就使用这种方式，canal 伪装了一个 Slave 与 Master 进行同步。

- Canal 模拟 MySQL Slave 的交互协议，伪装自己为 MySQL Slave，向 MySQL Master 发送 dump 协议。
- MySQL Master 收到 dump 请求，开始推送 binary log 给 Slave - 即 Canal。
- Canal 解析 Binary Log 对象（原始为 byte 流)。
- 在使用 canal 时，MySQL 需要开启 binlog，并且 binlog-format 必须为 row，可以在 mysql 的 my.cnf 文件中增加如下配置：

```mysql
log-bin=E:/mysql5.5/bin_log/mysql-bin.log
binlog-format=ROW
server-id=123
```

- 部署 canal 的服务端，配置 canal.properties 文件，然后　启动 bin/startup.sh 或 bin/startup.bat

```properties
# 设置要监听的 mysql 服务器的地址和端口
canal.instance.master.address = 127.0.0.1:3306
# 设置一个可访问 mysql 的用户名和密码并具有相应的权限，本示例用户名、密码都为 canal
canal.instance.dbUsername = canal
canal.instance.dbPassword = canal
# 连接的数据库
canal.instance.defaultDatabaseName = test
# 订阅实例中所有的数据库和表
canal.instance.filter.regex = .*\\..*
# 连接 canal 的端口
canal.port= 11111
# 监听到的数据变更发送的队列
canal.destinations= example
```

- 客户端开发，在 maven 中引入 Canal 的依赖

```xml
<dependency>
  <groupId>com.alibaba.otter</groupId>
  <artifactId>canal.client</artifactId>
  <version>1.0.21</version>
</dependency>
```

Canal java 客户端样例：https://github.com/alibaba/canal/wiki/ClientExample 

Canal Github 地址：https://github.com/alibaba/canal/



#### 4. 拦截器 + Kakfa

每次出现数据变化时，使用拦截器拦截 MySQL 变动信息，把对应 MySQL 的变动信息发到 Kafka 消息队列被消费，从而对 Couchbase 数据库进行同样的变动。

其中，变动信息可以是 “表名 + ID + 更新信息” 或者 ”insert、update 和 delete 的 SQL 语句”。

| 方案名                               | 例子                        | 优劣 |
| ------------------------------------ | --------------------------- | ---- |
| 表名 + ID + 更新信息                 | order, 3253523532, status=1 | -    |
| insert、update 和 delete 的 SQL 语句 | Delete from orders;         | -    |



#### 5. 根据开源的数据同步代码来直接改写



****

### ETL 工具



#### 1. Kettle

可以集成到 Java 项目中使用。



#### 2. Datax

阿里开源的 ETL 工具，实现包括 MySQL、Oracle、SqlServer、Postgre、HDFS、Hive、ADS、HBase、TableStore(OTS)、MaxCompute(ODPS)、DRDS 等各种异构数据源之间高效的数据同步功能，采用 java + python 进行开发，核心是 java 语言实现。

Github 地址：https://github.com/alibaba/DataX  

数据交换通过 DataX 进行中转，任何数据源只要和 DataX 连接上即可以和已实现的任意数据源同步。

优势：

- 每种插件都有自己的数据转换策略，放置数据失真；
- 提供作业全链路的流量以及数据量运行时监控，包括作业本身状态、数据流量、数据速度、执行进度等；
- 由于各种原因导致传输报错的脏数据，DataX可以实现精确的过滤、识别、采集、展示，为用户提过多种脏数据处理模式；
- 精确的速度控制；
- 健壮的容错机制，包括线程内部重试、线程级别重试。



#### 3. Debezium + Bireme

Debezium 是一个开源项目，为捕获数据更改 (change data capture,CDC) 提供了一个低延迟的流式处理平台。可以安装并且配置 Debezium 去监控数据库，然后应用就可以消费对数据库的每一个行级别 (row-level) 的更改。只有已提交的更改才是可见的，所以应用不用担心事务 (transaction) 或者更改被回滚 (roll back)。Debezium 为所有的数据库更改事件提供了一个统一的模型，所以应用不用担心每一种数据库管理系统的错综复杂性。另外，由于 Debezium 用持久化的、有副本备份的日志来记录数据库数据变化的历史，因此，应用可以随时停止再重启，而不会错过它停止运行时发生的事件，保证了所有的事件都能被正确地、完全地处理掉。

GitHub 地址为：https://github.com/debezium/debezium

本来监控数据库，并且在数据变动的时候获得通知其实一直是一件很复杂的事情。关系型数据库的触发器可以做到，但是只对特定的数据库有效，而且通常只能更新数据库内的状态(无法和外部的进程通信)。一些数据库提供了监控数据变动的API或者框架，但是没有一个标准，每种数据库的实现方式都是不同的，并且需要大量特定的知识和理解特定的代码才能运用。确保以相同的顺序查看和处理所有更改，同时最小化影响数据库仍然非常具有挑战性。

Debezium 正好提供了模块做这些复杂的工作。一些模块是通用的，并且能够适用多种数据库管理系统，但在功能和性能方面仍有一些限制。另一些模块是为特定的数据库管理系统定制的，所以他们通常可以更多地利用数据库系统本身的特性来提供更多功能。

Debezium 提供了对 MongoDB，mysql，pg，sqlserver 的支持。

Debezium 是一个捕获数据更改 (CDC) 平台，并且利用 Kafka 和 Kafka Connect 实现了自己的持久性、可靠性和容错性。

每一个部署在 Kafka Connect 分布式的、可扩展的、容错性的服务中的 connector 监控一个上游数据库服务器，捕获所有的数据库更改，然后记录到一个或者多个 Kafka topic (通常一个数据库表对应一个 kafka topic)。Kafka 确保所有这些数据更改事件都能够多副本并且总体上有序 (Kafka 只能保证一个 topic 的单个分区内有序)，这样，更多的客户端可以独立消费同样的数据更改事件而对上游数据库系统造成的影响降到很小 (如果 N 个应用都直接去监控数据库更改，对数据库的压力为 N，而用 debezium 汇报数据库更改事件到 kafka，所有的应用都去消费 kafka 中的消息，可以把对数据库的压力降到 1)。另外，客户端可以随时停止消费，然后重启，从上次停止消费的地方接着消费。每个客户端可以自行决定他们是否需要 exactly-once 或者 at-least-once 消息交付语义保证，并且所有的数据库或者表的更改事件是按照上游数据库发生的顺序被交付的。

对于不需要或者不想要这种容错级别、性能、可扩展性、可靠性的应用，他们可以使用内嵌的 Debezium connector 引擎来直接在应用内部运行 connector。这种应用仍需要消费数据库更改事件，但更希望 connector 直接传递给它，而不是持久化到 Kafka 里。

更详细的介绍可以参考：https://www.jianshu.com/p/f86219b1ab98

bireme 的github 地址 https://github.com/HashDataInc/bireme

bireme 的介绍：https://github.com/HashDataInc/bireme/blob/master/README_zh-cn.md

另外 Maxwell 也是可以实现 MySQL 到 Kafka 的消息中间件，消息格式采用 Json：

Download:
https://github.com/zendesk/maxwell/releases/download/v1.22.5/maxwell-1.22.5.tar.gz 

Source:
https://github.com/zendesk/maxwell 



#### 4. Databus

Databus 是一个实时的、可靠的、支持事务的、保持一致性的数据变更抓取系统。 2011 年在 LinkedIn正式进入生产系统，2013年开源。

Databus 通过挖掘数据库日志的方式，将数据库变更实时、可靠的从数据库拉取出来，业务可以通过定制化client实时获取变更。

Databus 的传输层端到端延迟是微秒级的，每台服务器每秒可以处理数千次数据吞吐变更事件，同时还支持无限回溯能力和丰富的变更订阅功能。

github：https://github.com/linkedin/databus

来源独立：Databus 支持多种数据来源的变更抓取，包括 Oracle 和 MySQL。

可扩展、高度可用：Databus 能扩展到支持数千消费者和事务数据来源，同时保持高度可用性。

事务按序提交：Databus 能保持来源数据库中的事务完整性，并按照事务分组和来源的提交顺寻交付变更事件。

低延迟、支持多种订阅机制：数据源变更完成后，Databus 能在微秒级内将事务提交给消费者。同时，消费者使用 Databus 中的服务器端过滤功能，可以只获取自己需要的特定数据。

无限回溯：这是 Databus 最具创新性的组件之一，对消费者支持无限回溯能力。当消费者需要产生数据的完整拷贝时（比如新的搜索索引），它不会对数据库产生任何额外负担，就可以达成目的。当消费者的数据大大落后于来源数据库时，也可以使用该功能。

Databus 社区 wiki 主页：[https://github.com/linkedin/Databus/wiki](https://github.com/linkedin/databus/wiki)



#### 5. Flinkx

FlinkX 是一款基于 Flink 的分布式离线/实时数据同步插件，可实现多种异构数据源高效的数据同步，其由袋鼠云于 2016 年初步研发完成，目前有稳定的研发团队持续维护，已在 Github 上开源（开源地址详见文章末尾）。并于今年 6 年份，完成批流统一，离线计算与流计算的数据同步任务都可基于 FlinkX 实现。

Github 地址：https://github.com/DTStack/flinkx

FlinkX 是一个基于 Flink 的批流统一的数据同步工具，既可以采集静态的数据，比如 MySQL，HDFS 等，也可以采集实时变化的数据，比如 MySQL binlog，Kafka 等。



#### 6. Apache NIFI

NIFI 可以处理各种各样的数据源和不同格式的数据。可以从一个源中获取数据，对其进行转换，然后将其推送到另一个目标存储地。



#### 7. Streamsets

StreamSets 数据收集器是一个轻量级，强大的引擎，实时流数据。使用 Data Collector 在数据流中路由和处理数据。



#### 8. FLink SQL CDC

官方详细说明：https://ci.apache.org/projects/flink/flink-docs-release-1.11/zh/dev/table/connectors/formats/canal.html



#### 总结

1、Databus 活跃度不高，datax 和 canal 相对比较活跃。

2、Datax 一般比较适合于全量数据同步，对全量数据同步效率很高（任务可以拆分，并发同步，所以效率高），对于增量数据同步支持的不太好（可以依靠时间戳 + 定时调度来实现，但是不能做到实时，延迟较大）。

3、Canal 、Databus 等由于是通过日志抓取的方式进行同步，所以对增量同步支持的比较好。

4、以上这些工具都缺少一个监控和任务配置调度管理的平台来进行支撑。

5、Flinkx 活跃度也不是非常高，关注的人还不是很多。

6、Apache NIFI 适合于重量级的数据同步处理，Datax 相对来说比较轻量级。 

7、Flink sql cdc 未来会很有前景。





****

### 贤银的想法

通过MQ实现数据异构，实现其他业务查询。
例如：订单系统，根据用户维度分片存储。通过MQ消息把用户订单分发到商家系统。商家系统根据商家维度分片存储系统。
好处：1.用户订单形成闭环，即使商家系统无法正常使用，也不影响用户下单。其他服务也可以通过类似方式实现各自服务隔离以及更好飞服务下游服务
          2根据商家数据量独立做数据存储（类似租借模式）网络分流等等。
建议全查询使用搜索引擎如：es。

canal + otter +MQ 

目前canal支持mixed,row,statement多种日志协议的解析，
如果是多机房，需要配合otter进行数据库同步，目前仅支持row协议的同步，使用时需要注意.
canal 模拟 MySQL slave 的交互协议，伪装自己为 MySQL slave ，向 MySQL master 发送dump 协议
MySQL master 收到 dump 请求，开始推送 binary log 给 slave (即 canal )
canal 解析 binary log 对象(原始为 byte 流)

DataX 数据同步工具（定时任务）



****







ETL  -> Kettle -> PDI



kettle 和 Spark

